
<?php include('server.php') ?>
<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700;800;900&display=swap" rel="stylesheet">
    <title>Dashboard</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">

       <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" 
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

         <link rel="stylesheet" href="../font/font/flaticon.css">
      <link rel="stylesheet" href="../css/dash.css" type="text/css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      <script type="text/javascript" src="../script/dash.js"></script>
      <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() {
              var data = google.visualization.arrayToDataTable([
                  ['Day', 'Sales' ],
                  ['Mon',  1000      ],
                  ['Tue',  1170],
                  ['Wed',  660],
                  ['Thus',  1030],
                  ['Fri',  1230],
                  ['Sat',  1530],
                  ['Sun',  930]

              ]);

              var options = {
                  title: 'Daily Sales',
                  curveType: 'function',
                  legend: { position: 'bottom' }
              };

              var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

              chart.draw(data, options);
          }
      </script>

  </head>
  


  
  <body>
  
  
  <div id="wrapper">
   <div class="overlay"></div>
    

    <nav class="fixed-top align-top" id="sidebar-wrapper" role="navigation">
       <div class="simplebar-content" style="padding: 0px;">
                <a class="sidebar-brand" href="#">
          <span class="align-middle">Dashboard</span>
        </a>

                 <ul class="navbar-nav align-self-stretch">
     
<li class="sidebar-header">
                        Pages
                    </li>
    <li class=""> 
          <a class="nav-link text-left active"  role="button" 
          aria-haspopup="true" aria-expanded="false">
       <i class="flaticon-bar-chart-1"></i>  Dashboard 
         </a>
          </li>
     
       <li class="has-sub"> 
          <a class="nav-link collapsed text-left" href="#collapseExample2" role="button" data-toggle="collapse" >
        <i class="flaticon-user"></i>   Dealer/ Commission Agent
         </a>
          <div class="collapse menu mega-dropdown" id="collapseExample2">
        <div class="dropmenu" aria-labelledby="navbarDropdown">
        <div class="container-fluid ">
                            <div class="row">
                                <div class="col-lg-12 px-2">
                                    <div class="submenu-box"> 
                                        <ul class="list-unstyled m-0">
                                            <li><a href="#" data-toggle="modal"  data-target="#myModal1">Add Dealer/ Commission Agent</a></li>
                                            <li><a href="#" data-toggle="modal"  data-target="#myModal2">Dealer/ Commission Agent Details</a></li>
                                           




                                        </ul>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
             </div>
          </div>

          </li>


           <li class="has-sub"> 
          <a class="nav-link collapsed text-left" href="#collapseExample2" role="button" data-toggle="collapse" >
        <i class="flaticon-user"></i>   Admin
         </a>
          <div class="collapse menu mega-dropdown" id="collapseExample2">
        <div class="dropmenu" aria-labelledby="navbarDropdown">
        <div class="container-fluid ">
                            <div class="row">
                                <div class="col-lg-12 px-2">
                                    <div class="submenu-box"> 
                                        <ul class="list-unstyled m-0">
                                            <li><a href="#" data-toggle="modal"  data-target="#myModal4">Add Admin</a></li>
                                            <li><a href="#" data-toggle="modal"  data-target="#myModal5">Admin Deatails</a></li>
                                            




                                        </ul>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
             </div>
          </div>
          
          </li>
          
         
          
          </ul>


                
            </div>
       
       
    </nav>












        <div id="page-content-wrapper">
         
            
            <div id="content">

       <div class="container-fluid p-0 px-lg-0 px-md-0">

        <nav class="navbar navbar-expand navbar-light my-navbar">


            <div type="button"  id="bar" class="nav-icon1 hamburger animated fadeInLeft is-closed" data-toggle="offcanvas">
               <span></span>
                <span></span>
                 <span></span>
            </div>
            


          <form class="d-none d-sm-inline-block form-inline navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light " placeholder="Search for..." aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>


          <ul class="navbar-nav ml-auto">


            <li class="nav-item dropdown  d-sm-none">
         

              <div class="dropdown-menu dropdown-menu-right p-3">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small"
                    placeholder="Search for..." >
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>


           <li class="nav-item dropdown">
                            <a class="nav-icon dropdown" href="#" id="alertsDropdown" data-toggle="dropdown" aria-expanded="false">
                                <div class="position-relative">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell align-middle"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg>
                                    <span class="indicator">0</span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right py-0" aria-labelledby="alertsDropdown">
                                <div class="dropdown-menu-header">
                                    0 New Notifications
                                </div>
                                <div class="list-group">
                                    <a href="#" class="list-group-item">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle text-danger"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12.01" y2="16"></line></svg>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">Notification Header</div>
                                                <div class="text-muted small mt-1">Restart server 12 to complete the update.</div>
                                                <div class="text-muted small mt-1">30m ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    
                                </div>
                                <div class="dropdown-menu-footer">
                                    <a href="#" class="text-muted">Show all notifications</a>
                                </div>
                            </div>
                        </li>

            <li class="nav-item">
              <a class="nav-link " href="#"
             role="button">
                <i class="fas fa-envelope"></i>

                <span class="badge badge-danger badge-counter">0</span>
              </a>
            </li>


            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Dashboard</span>
                <img class="img-profile rounded-circle" src="#" alt="Logo">
              </a>
            </li>

          </ul>

        </nav>

        <div class="container-fluid px-lg-4">
<div class="row">
<div class="col-md-12 mt-lg-4 mt-4">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> 
            Generate Report</a>
          </div>
          </div>
<div class="col-md-12">
    <div id="myModal1" class="modal fade" role="dialog" >
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content" style="height: 500px; width: 700px;opacity: 0.9">
                                <div class="modal-header">
                                   
                                    
                                    
                                </div>
                                <div class="modal-body">
                                    <h2>Add Dealer/ Commission Agent</h2>
    <form method="post" action="#">

        <?php include('errors.php'); ?>

           
            <input type="text" placeholder="Username" name="link"   id="input-form" required></br>
    
    
            <input type="text" placeholder="Password" name="name"   id="input-form" required></br>
    

            <button type="submit" class="primary-btn" name="update">Add Dealer/ Commission Agent</button>
             
        </div>
        

    </form>

                                </div>
                                <div class="modal-footer">
                                    
                                </div>
                            </div>

                        </div>
                    </div>
            <div id="myModal2" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content"  style="height: 500px; width: 700px;opacity: 0.9">
                                <div class="modal-header" style="text-align: center">
                                    
                                    
                                </div>
                                <div class="modal-body">
                                   

    <section class="services-section spad">

                    <div class="section-title">
                        <h2>Dealer/ Commission Agent</h2>
                       
                 
                    

<table class="tblone">
    <tr>
  
      <th>id</th>
       <th>Username</th>
      <th>Password</th>
       <th>Delete</th>
    </tr>

   
 <?php

  $username = "";
  $email    = "";
  $errors = array(); 
  $_SESSION['success'] = "";

  // connect to database
  $con = mysqli_connect('localhost', 'root', '', 'dash');

 $q = "select * from update_section";

 $query = mysqli_query($con,$q);

 while($res = mysqli_fetch_array($query)){
 ?>
 <tr>
 <td> <?php echo $res['id'];  ?> </td>
 <td> <?php echo $res['name'];  ?> </td>
 <td><i class="fa fa-trash" aria-hidden="true"></i>
</td>

 </tr>

 <?php 
 }
 ?>

  </table>
                       
                    </div>
                </div>
              
            </div>
        </div>
    </section>
    
                                </div>
                                <div class="modal-footer">
                                   
                                </div>
                            </div>

                        </div>
                    </div>
       
       <div id="myModal8" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content" style="height: 500px; width: 700px;opacity: 0.9">
                                <div class="modal-header">
                                   
                                    
                                </div>
                                <div class="modal-body">
                                                                  
    <section class="services-section spad">

                    <div class="section-title">
                        <h2>Add Admin<h2>
                       
                 
 <form method="post" action="#">

        <?php include('errors.php'); ?>

        
            <input type="text" placeholder="Username" name="state_link"   id="input-form" required></br>
    
    
            <input type="text" placeholder="Password" name="state_name"   id="input-form" required></br>
    

            <button type="submit" class="primary-btn" name="state">Add Admin</button>
             
        </div>
        

    </form>
                       
                    </div>
                </div>
              
            </div>
        </div>
    </section>
                                </div>
                                <div class="modal-footer">
                                    
                                </div>
                            </div>

                        </div>
                    </div>


                     <div id="myModal4" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content" style="height: 500px; width: 700px;opacity: 0.9">
                                <div class="modal-header">
                                   
                                    
                                </div>
                                <div class="modal-body">
                                                                  
    <section class="services-section spad">

                    <div class="section-title">
                        <h2>Add Admin</h2>
                       
                 
 <form method="post" action="#">

        <?php include('errors.php'); ?>

           
            <input type="text" placeholder="Username" name="job_link"   id="input-form" required></br>
    
    
            <input type="text" placeholder="Password" name="job_name"   id="input-form" required></br>
    

            <button type="submit" class="primary-btn" name="job">Add Admin</button>
             
        </div>
        

    </form>
                       
                    </div>
                </div>
              
            </div>
        </div>
    </section>
                                </div>
                                <div class="modal-footer">
                                    
                                </div>
                            </div>

                        </div>
                    </div>
      <div id="myModal5" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content" style="height: 500px; width: 700px;opacity: 0.9">
                                <div class="modal-header">
                                   
                                   
                                </div>
                                <div class="modal-body">
                                 <h2>Admin Details</h2>
                       
                 
                    

<table class="tblone">
    <tr>
  
      <th>id</th>
       <th>Username</th>
       <th>Password</th>
      <th>Delete</th>
    </tr>

   
 <?php

  $username = "";
  $email    = "";
  $errors = array(); 
  $_SESSION['success'] = "";

  // connect to database
  $con = mysqli_connect('localhost', 'root', '', 'dash');

 $q = "select * from job_section";

 $query = mysqli_query($con,$q);

 while($res = mysqli_fetch_array($query)){
 ?>
 <tr>
 <td> <?php echo $res['id'];  ?> </td>
 <td> <?php echo $res['job_name'];  ?> </td>
 <td><i class="fa fa-trash" aria-hidden="true"></i>
</td>

 </tr>

 <?php 
 }
 ?>

  </table>
                                </div>
                                <div class="modal-footer">
                                    
                                </div>
                            </div>

                        </div>
                    </div>
   
 
    <div class="row">
        <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header card-header-success">
                    <div id="curve_chart" ></div>
                </div>
                <div class="card-body">
                    <h4 class="card-title">Daily web traffic</h4>
                    <p class="card-category">
                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> 0% </span> increase in today sales.</p>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">access_time</i> updated  justnow
                    </div>
                </div>
            </div>
        </div>
       
        <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header card-header-danger">
                    <div id="piechart" ></div>
                </div>
                <div class="card-body">
                    <h4 class="card-title">Link visited by user</h4>
                    <p class="card-category">0</p>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">access_time</i> Updated justnow
                    </div>
                </div>
            </div>
        </div>
    </div>
               <footer class="footer">
                <div style="text-align: center">
                   
                </div>
            </footer>
            
        </div>
        </div>


    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    
  
 <script>
 
$('#bar').click(function(){
    $(this).toggleClass('open');
    $('#page-content-wrapper ,#sidebar-wrapper').toggleClass('toggled' );

});
  </script>

   
   
 
       </div>
            </div>
        </div>
  </div>
  
  </body>
</html>
