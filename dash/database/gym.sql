CREATE DATABASE gym;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `email` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
 `username` varchar(100) NOT NULL,
 `password` varchar(100) NOT NULL,
 `contact` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `payment_id` int(100) NOT NULL,
 `member_id` int(100) NOT NULL,
  `payment_time` varchar(100) NOT NULL,
  `payment_date` varchar(100) NOT NULL,
  `user_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `membershiptype` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `type_id` int(100) NOT NULL ,
 `type_name` varchar(100) NOT NULL ,
  `membership_period` varchar(100) NOT NULL,
  `membership_amount` varchar(100) NOT NULL,
 `user_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `instructor` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `instructor_id` int(100) NOT NULL ,
 `instructor_name` varchar(100) NOT NULL ,
  `contact` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
 `user_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `workoutplan` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `workout_name` int(100) NOT NULL ,
   `workouttype` int(100) NOT NULL ,
 `startingtime` varchar(100) NOT NULL ,
  `endingtime` varchar(100) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `name` varchar(100) NOT NULL ,
  `email` varchar(100) NOT NULL ,
   `subject` varchar(100) NOT NULL ,
  `message` varchar(100) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `workout` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `workout_id` int(100) NOT NULL ,
 `workout_name` varchar(100) NOT NULL ,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `name` varchar(100) NOT NULL ,
  `email` varchar(100) NOT NULL ,
   `subject` varchar(100) NOT NULL ,
  `message` varchar(100) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


